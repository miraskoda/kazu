import React from 'react';
import {Link} from 'react-router-dom';
import AppName from './AppName/AppName';
import {appName} from '../../constants';
import {UserConsumer} from '../../context/UserContext';

class ApplicationBar extends React.Component {
    render() {
        return (
            <nav className={'navbar navbar-expand-lg navbar-light bg-light'}>
                <Link className="navbar-brand" to={'/'}>Kazu</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className={'collapse navbar-collapse'} id="navbarSupportedContent">
                    <ul className={'navbar-nav mr-auto'}>
                        <UserConsumer>
                            {
                                ({username, email}) => (
                                    <AppName
                                        username={username}
                                        name={appName}
                                        email={email}
                                    />
                                )
                            }
                        </UserConsumer>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default ApplicationBar;
