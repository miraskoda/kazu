import React from 'react';
import PropTypes from 'prop-types';

const AppName = ({email, username, name}) => {
    return (
        <li className={'nav-item'}>
            <span>{name}</span>
            <span> - </span>
            {
                username ? (
                    <span>Aktuální Uživatel: {username}</span>
                ) : (
                    <span>Nepřihlášen</span>
                )
            }
        </li>
    )
};

AppName.propTypes = {
    name: PropTypes.string,
};

export default AppName;
