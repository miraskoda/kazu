import React from 'react';
import PropTypes from 'prop-types';

const FormLogin = ({ username, password, handleChange, handleSubmit }) => {
    return (
        <form onSubmit={handleSubmit}>

            <div className="form-group">

                <label>
                    Uživatelské jméno:

                </label>

                <input
                    className={'form-control ml-2'}
                    type="text"
                    name="username"
                    value={username}
                    onChange={handleChange('username')}
                />

            </div>

            <div className="form-group">

                <label>
                    Heslo:
                </label>

                <input
                    className={'form-control ml-2'}
                    type="password"
                    name="password"
                    value={password}
                    onChange={handleChange('password')}
                />

            </div>


            <button className={'btn btn-primary'} type={'submit'}>Přihlásit</button>

        </form>
    );
};

FormLogin.propTypes = {
    username: PropTypes.string,
    password: PropTypes.string,
    handleChange: PropTypes.func,
    handleSubmit: PropTypes.func,
};

export default FormLogin;
