import React from 'react';

const PersonalData = ({personalData}) => {
    return (
        <div>
            <ul className={'list-group'}>
                {
                    personalData.map(({title, text}) => (
                            <li className={'list-group-item'} key={title}>
                                <div className="card" style={{width: '18rem'}}>
                                    <div className="card-body">
                                        <h5 className="card-title">{title}</h5>
                                        <p className="card-text">{text}</p>
                                    </div>
                                </div>
                            </li>
                        )
                    )
                }
            </ul>
        </div>
    );
};

export default PersonalData;
