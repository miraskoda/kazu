import Medicaments from './Medicaments';
import Exams from './Exams';
import PersonalData from './PersonalData';

export { Medicaments, Exams, PersonalData };
