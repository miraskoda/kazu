import React from 'react';

const Exams = ({ examsData }) => {
    return (
        <div>
            <ul className={'list-group'}>
                    {
                        examsData.map(
                            (exam) => <li className={'list-group-item'} key={exam.title}>{exam.title}</li>
                        )
                    }
            </ul>
        </div>
    );
};

export default Exams;
