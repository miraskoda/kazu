import React, {Component} from 'react';
import ApplicationBar from './components/ApplicationBar';
import {Patient, Result, Login} from './pages';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {UserProvider} from './context/UserContext';
import ProtectedRoute from './Routes/ProtectedRoute';

class App extends Component {
    render() {
        return (
            <UserProvider>
                <BrowserRouter>
                    <div style={{width: '100%'}}>
                    <ApplicationBar/>
                        <Switch>
                            <ProtectedRoute exact path={'/'} component={Patient}/>
                            <Route path={'/login'} component={Login}/>
                            <ProtectedRoute path={'/result/:choice'} component={Result}/>
                        </Switch>
                        </div>
                    </BrowserRouter>
            </UserProvider>
        );
    }
}

export default App;
