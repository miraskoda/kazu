export const appName = 'Aplikace pro LF';

export const patientsUrl = 'https://owe-kazu.herokuapp.com/api/rest/student';

export const resultUrl = 'https://owe2019.herokuapp.com/result';

export const loginUrl = 'https://owe2019.herokuapp.com/login';
