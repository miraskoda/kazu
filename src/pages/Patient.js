import React from 'react';
import Section from '../components/Section';
import {patientsUrl} from '../constants';
import {PersonalData, Exams} from '../components/PacientInformation';

class Patient extends React.Component {
    state = {
        patient: {},
    };

    componentDidMount() {
        fetch(patientsUrl)
            .then((response) => response.json())
            .then((jsonResponse) => {
                this.setState({patient: jsonResponse})
            }).catch((err) => console.error(err));
    }

    render() {
        const { patient } = this.state;
        const properties = patient.properties || [];
        const personalData = properties.filter((value) => !value.exam);
        const examsData = properties.filter((value) => value.exam);
        return (
            <div className="row">
                <div className={'col-sm'}>
                    <Section heading={'Údaje'} body={
                        <PersonalData personalData={personalData}/>
                    }/>
                </div>
                <div className={'col-sm'}>
                    <Section heading={'Pokusy'} body={
                        <Exams examsData={examsData}/>
                    }/>
                </div>
            </div>
        );
    }
}

export default Patient;
