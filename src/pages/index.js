import Result from './Result';
import Patient from './Patient';
import Login from './Login';

export { Result, Patient, Login };
